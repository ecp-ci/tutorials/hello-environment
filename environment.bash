#!/usr/bin/env bash

set -o pipefail

echo "whoami: $(whoami)"
echo "hostname: $(hostname)"
echo "uname: $(uname -a)"
echo "module avail"
module avail
